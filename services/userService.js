const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // DONE: Implement methods to work with user
    
    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            throw Error('No access to database');
        }
        return users;
    }

    getUser(id) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw Error(`User with id="${id}" not found`);
        }
        return user;
    }

    createUser(newUserData) {
        const newUser = UserRepository.create(newUserData);
        if (!newUser) {
            throw Error('New user was not created');
        }
        return newUser;
    }

    updateUser(id, data) {
        this.getUser(id);
        const user = UserRepository.update(id, data);
        if (!user) {
            throw Error(`User with id="${id}" was not updated`);
        }
        return user;
    }

    deleteUser(id) {
        this.getUser(id);
        const wasUser = UserRepository.delete(id);
        if (!wasUser.length) {
            throw Error(`User with id="${id}" was not deleted`);
        }
        return wasUser[0];
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();