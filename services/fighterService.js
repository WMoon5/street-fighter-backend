const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // DONE: Implement methods to work with fighters
    
    getAllFighters() {
        const fighters = FighterRepository.getAll();
        
        if (!fighters) {
            throw Error('No access to database');
        }
        
        return fighters;
    }

    createFighter(data) {
        const newFighter = FighterRepository.create(data);
        
        if (!newFighter) {
            throw Error('No access to database');
        }
        
        return newFighter;
    }

    getFighter(id) {
        const fighter = FighterRepository.getOne({ id });
        
        if (!fighter) {
            throw Error(`Fighter with id="${id}" not found`);
        }
        
        return fighter;
    }

    updateFighter(id, data) {
        this.getFighter(id);
        const fighter = FighterRepository.update(id, data);
        
        if (!fighter) {
            throw Error(`Fighter with id="${id}" was not updated`);
        }
        
        return fighter;
    }

    deleteFighter(id) {
        this.getFighter(id);
        const wasFighter = FighterRepository.delete(id);
        
        if (!wasFighter.length) {
            throw Error(`Fighter with id="${id}" was not deleted`);
        }
        
        return wasFighter[0];
    }
}

module.exports = new FighterService();