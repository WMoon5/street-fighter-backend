const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // DONE: Implement validatior for fighter entity during creation
    if (
        req.body &&
        req.body.hasOwnProperty('name') &&
        typeof req.body.power === 'number' &&
        req.body.power < 100 &&
        req.body.power > 0 &&
        typeof req.body.defense === 'number' &&
        req.body.defense < 100 &&
        req.body.defense > 0 &&
        !req.body.hasOwnProperty('id')
    ) {
        let newBody = Object.assign({}, fighter);
        delete newBody['id'];
        Object.keys(newBody).forEach((key) => {
            if (req.body.hasOwnProperty(key)) {
                newBody[key] = req.body[key]
            }
        });
        req.body = newBody;
                
    } else {
        res.err = new Error('Not valid input for Fighter entity creation');
    }
    
    next();
}

const updateFighterValid = (req, res, next) => {
    // DONE: Implement validatior for fighter entity during update
    const isPowerValid = (req.body.hasOwnProperty('power')) ? (
        typeof req.body.power === 'number' &&
        req.body.power < 100 &&
        req.body.power > 0
    ) : true;
    
    const isDefenceValid = (req.body.hasOwnProperty('defence')) ? (
        typeof req.body.defense === 'number' &&
        req.body.defense < 100 &&
        req.body.defense > 0
    ) : true;
    
    if (
        req.body &&
        isPowerValid &&
        isDefenceValid &&
        !req.body.hasOwnProperty('id')
    ) {
        let testBody = Object.assign({}, fighter);
        delete testBody['id'];
        Object.keys(req.body).forEach((key) => {
            if (!testBody.hasOwnProperty(key)) {
                delete req.body[key];
            }
        });
                
    } else {
        res.err = new Error('Not valid input for Fighter entity update');
    }
    
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;