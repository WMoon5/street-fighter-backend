const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // DONE: Implement validatior for user entity during creation
    
    // email is required
    const isGmail = (req.body.hasOwnProperty('email')) ?
        (req.body.email.toLowerCase().indexOf('@gmail.com') > -1) : false;
    // phone is optional
    const isUAphone = (req.body.hasOwnProperty('phoneNumber')) ?
        (req.body.phoneNumber.indexOf('+380') > -1) : false;
    // password is required
    const isPswdLong = (req.body.hasOwnProperty('password')) ?
        (req.body.password.length >= 3) : false;
    
    if (req.body &&
        isGmail &&
        isUAphone &&
        isPswdLong &&
        req.body.hasOwnProperty('firstName') &&
        req.body.hasOwnProperty('lastName') &&
        !req.body.hasOwnProperty('id')
    ) {
        let newBody = Object.assign({}, user);
        delete newBody['id'];
        Object.keys(newBody).forEach((key) => {
            if (req.body.hasOwnProperty(key)) {
                newBody[key] = req.body[key]
            }
        });
        req.body = newBody;
                
    } else {
        res.err = new Error('Not valid input for User entity creation');
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // DONE: Implement validatior for user entity during update
    // some fields might be missing in update data
    const isGmail = (req.body.hasOwnProperty('email')) ?
        (req.body.email.toLowerCase().indexOf('@gmail.com') > -1) : true;
    const isUAphone = (req.body.hasOwnProperty('phoneNumber')) ?
        (req.body.phoneNumber.indexOf('+380') > -1) : true;
    const isPswdLong = (req.body.hasOwnProperty('password')) ?
        (req.body.password.length >= 3) : true;
    
    if (req.body &&
        isGmail &&
        isUAphone &&
        isPswdLong &&
        !req.body.hasOwnProperty('id')
    ) {
        let testBody = Object.assign({}, user);
        delete testBody['id'];
        Object.keys(req.body).forEach((key) => {
            if (!testBody.hasOwnProperty(key)) {
                delete req.body[key];
            }
        });
                
    } else {
        res.err = new Error('Not valid input for User entity update');
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;