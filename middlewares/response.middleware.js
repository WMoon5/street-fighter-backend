const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   
   if (!res.err) {
   
      res.status(200).json(res.data);
   
   } else {
   
      const err = {
         error: true,
         message: res.err.message
      }
      
      if (res.data) {
         res.status(404).json(err);
      
      } else {
         res.status(400).json(err);
      }
   }
   
}

exports.responseMiddleware = responseMiddleware;