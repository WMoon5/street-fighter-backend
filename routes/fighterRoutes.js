const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// DONE: Implement route controllers for fighter

router.get('/', (req, res, next) => {
    try {
        res.data = FighterService.getAllFighters();

    } catch (err) {
        res.err = err;

    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = FighterService.createFighter(req.body);
        }
    } catch (err) {
        res.err = err;

    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = FighterService.getFighter(req.params.id);
        
    } catch (err) {
        res.err = err;

    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = FighterService.updateFighter(req.params.id, req.body);
        }
    } catch (err) {
        res.err = err;

    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = FighterService.deleteFighter(req.params.id);

    } catch (err) {
        res.err = err;
        res.data = {};

    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;