const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// DONE: Implement route controllers for user
router.get('/', (req, res, next) => {
    try {
        res.data = UserService.getUsers();

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = UserService.getUser(req.params.id);

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = UserService.createUser(req.body);
        }       
    } catch (err) {
        res.err = err;
    
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = UserService.updateUser(req.params.id, req.body);
        }       
    } catch (err) {
        res.err = err;
    
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = UserService.deleteUser(req.params.id);

    } catch (err) {
        res.err = err;
        res.data = {};
        
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;